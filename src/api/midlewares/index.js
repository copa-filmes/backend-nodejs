const compose = require('koa-compose')
const bodyparser = require('koa-bodyparser')
const koaHealthcheck = require('koa-simple-healthcheck')
const cors = require('@koa/cors')
const koaRespond = require('./koa-respond')
const routeAggregator = require('./routeAggregator')

module.exports = () =>
  compose([
    cors(),
    bodyparser(),
    koaHealthcheck(),
    koaRespond(),
    routeAggregator(),
  ])
