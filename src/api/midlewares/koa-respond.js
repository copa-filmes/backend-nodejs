const koaRespond = require('koa-respond')

module.exports = () =>
    koaRespond({
        ok: 200,
        internalServerError: 500,
    })