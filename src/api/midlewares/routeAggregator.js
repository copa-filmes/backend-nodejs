const compose = require('koa-compose')

const filmesRoutes = require('../../domain/filmes/routes')
const copaRoutes = require('../../domain/copa/routes')

module.exports = () =>
    compose([
        filmesRoutes,
        copaRoutes,
    ])