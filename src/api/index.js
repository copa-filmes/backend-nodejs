import api from './api'
import config from 'config'

const PORT = config.get('port')
const onListen = () => console.log(`listening on ${PORT}`)

const start = () =>
    api.listen(PORT, onListen)

export default start