const supertest = require('supertest')
const nock = require('nock')

const api = require('./api')
const { TIMES, GUERRA_INFINITA, OS_INCRIVEIS_2 } = require('../../tests/support/defaults')

describe('API', () => {
    const server = api.listen()
    const request = supertest(server)

    afterEach( () => server.close() );

    it('should respond 200 on /healthcheck', async () => {
        const { status } = await request.get('/healthcheck')
        
        expect(status).toBe(200)
    })

    it('should respond 404 on /non-existent-path', async () => {
        const { status } = await request.get('/non-existent-path')

        expect(status).toBe(404)
    })

    it('should respons 200 on /filmes', async () => {
        const mockApiResponse = [ { id: 'teste', titulo: 'Um filme', ano: 2010, nota: 9.5 }  ]

        nock('https://copadosfilmes.azurewebsites.net')
          .get('/api/filmes')
          .reply(200, mockApiResponse)

        const { status, body } = await request.get('/filmes')

        expect(status).toBe(200)
        expect(body).toMatchObject(mockApiResponse)
    })

    it('should respons 500 on /filmes if copadosfilmes api fail', async () => {
        nock('https://copadosfilmes.azurewebsites.net')
          .get('/api/filmes')
          .reply(500, { message: 'MOCK' })

        const { status } = await request.get('/filmes')

        expect(status).toBe(500)
    })

    it('should respond [VINGADORES, OS_INCRIVEIS] on POST /copa with items specified on PDF', async () => {
        const requestBody = TIMES
        const { status, body } = await request
            .post('/copa')
            .send(requestBody)

        expect(status).toBe(200)
        expect(body).toMatchObject([ GUERRA_INFINITA, OS_INCRIVEIS_2 ])
    })

    it("expect POST /copa to return 400 if body hasn't 8 items", async () => {
        const itemsToCut = [1,2,3,4,5,6,7,8]

        return itemsToCut.reduce(async (accumulator, item) => {
            await accumulator;

            const requestBody = TIMES.slice(item)

            const { status } = await request
                .post('/copa')
                .send(requestBody)

            expect(status).toBe(400)
        }, Promise.resolve())
    })
})