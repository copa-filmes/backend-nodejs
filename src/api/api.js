const Koa = require('koa')
const midlewares = require('./midlewares')

const api = new Koa()
api.use( midlewares() )

module.exports = api