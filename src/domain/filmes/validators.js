const { Joi } = require('koa-joi-router')

const Filme = Joi.object({
    id:	Joi.string(),
    titulo:	Joi.string(),
    ano: Joi.number().integer(),
    nota: Joi.number(),
})

const filmesOutput = Joi.array().items(Filme)



module.exports = {
    filmesOutput,
    Filme,
}