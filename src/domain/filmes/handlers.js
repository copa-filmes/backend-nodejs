const axios = require('axios')

const getFilmes = (context) =>
    axios.get('https://copadosfilmes.azurewebsites.net/api/filmes')
        .then( response => response.data )
        .then(context.ok)
        .catch(context.internalServerError)


module.exports = {
    getFilmes,
}