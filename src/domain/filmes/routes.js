const Router = require('koa-joi-router')
const handlers = require('./handlers')
const validators = require('./validators')

const routes = Router()

routes.prefix('/filmes')

routes.get('/', {
    validate: {
        output: {
            200: {
                body: validators.filmesOutput
            }
        }
    },
    handler: handlers.getFilmes
})


module.exports = routes.middleware()