const { Joi } = require('koa-joi-router')
const { Filme } = require('../filmes/validators')

const campeonatoInput = Joi.array().length(8).items(Filme)

const campeonatoOutput = Joi.array().length(2).items(Filme)

module.exports = {
    campeonatoInput,
    campeonatoOutput,
}