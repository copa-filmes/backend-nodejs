const Router = require('koa-joi-router')
const handlers = require('./handlers')
const validators = require('./validators')

const routes = Router()

routes.prefix('/copa')

routes.post('/', {
    validate: {
        type: 'json',
        body: validators.campeonatoInput,
        output: {
            200: {
                body: validators.campeonatoOutput
            }
        }
    },
    handler: handlers.processCopa
})


module.exports = routes.middleware()