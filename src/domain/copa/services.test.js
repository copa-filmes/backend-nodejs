const services = require('./services')
const {
    GUERRA_INFINITA,
    OS_INCRIVEIS_2,
    THOR_RAGNAROK,
    JURASSIC_WORLD_REINO_AMEAÇADO,
    DEADPOOL_2,
    HAN_SOLO,
    HEREDITÁRIO,
    OITO_MULHERES_E_UM_SEGREDO,
} = require('../../../tests/support/defaults')

describe('domain > copa > services', () => {
    describe('doPartida', () => {
        it('"Guerra Infinita" x "Os Incríveis 2" => "Guerra Infinita"', () => {
            const result = services.doPartida([GUERRA_INFINITA, OS_INCRIVEIS_2]) 

            expect(result).toMatchObject(GUERRA_INFINITA)
        })

        it('expect winner be defined by nome if nota are equal', () => {
            const times = [{nome: 'Zx', nota: 10}, { nome: 'Ax', nota: 10}]

            const result = services.doPartida(times)

            expect(result).toMatchObject(times[1])
        })
    })

    describe('doFase', () => {
        it('expect semifinal be as pdf definition (Vingadores, Thor, Incriveis, Jurassic World) => (Vingadores, Incriveis)', () => {
            const times = [GUERRA_INFINITA, THOR_RAGNAROK, OS_INCRIVEIS_2, JURASSIC_WORLD_REINO_AMEAÇADO]

            const result = services.doFase(times)

            expect(result).toMatchObject([GUERRA_INFINITA, OS_INCRIVEIS_2])
        })
    })

    describe('doOrganizarCopa', () => {
        it('[1,2,3,4,5,6,7,8] => [1,8,3,6,5,4,7,2]', () => {
            const result = services.doOrganizarCopa([1,2,3,4,5,6,7,8])
            expect(result).toEqual([1,8,2,7,3,6,4,5])
        })
    })

    describe('doCopa', () => {
        it('expect copa be as pdf definition (Deadpool, Han Solo, Hereditario, Jurassic, 8Mulheres1Segredo, Vingadores, Thor, Incriveis) => (Vingadores, Incriveis)', () => {
            const times = [ OS_INCRIVEIS_2,  JURASSIC_WORLD_REINO_AMEAÇADO, OITO_MULHERES_E_UM_SEGREDO, HEREDITÁRIO, GUERRA_INFINITA, DEADPOOL_2, HAN_SOLO, THOR_RAGNAROK]

            const result = services.doCopa(times)
            expect(result).toMatchObject([GUERRA_INFINITA, OS_INCRIVEIS_2])
        })
    })
})