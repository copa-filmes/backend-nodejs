const { doCopa } = require('./services')

const processCopa = (context) =>
    Promise.resolve(context.request.body)
        .then(doCopa)
        .then(context.ok)
        .catch(context.internalServerError)


module.exports = {
    processCopa,
}