const _ = require('lodash')

const orderMatch = ([filmeA, filmeB]) =>
    _.orderBy([filmeA, filmeB], ['nota', 'nome'], ['desc', 'asc'])

const doPartida = ([filmeA, filmeB]) =>
    _.chain([filmeA, filmeB])
        .thru(orderMatch)
        .head()
        .value()

const doFase = (filmes) =>
    _.chain(filmes)
        .chunk(2)
        .map(doPartida)
        .value()


const _splitIn2 = (filmes) => [
    filmes.slice(0, filmes.length/2),
    filmes.slice(-filmes.length/2),
]

const _reverseSecondArray = ([first, second]) => [first, _.reverse(second)]

const _mergeArraysAlterningItems =  ([first, second]) => _.flatMap(first, (item, index) => [ item, second[index] ] )

const doOrganizarCopa = _.flow(
    _splitIn2,
    _reverseSecondArray,
    _mergeArraysAlterningItems,
)

const processCopaUntilFinal = (filmes) =>
    filmes.length == 2 ?
        filmes :
        processCopaUntilFinal(doFase(filmes))

const doCopa = _.flow(
    doOrganizarCopa,
    processCopaUntilFinal,
    orderMatch, // order between champion and vice-champion
)

module.exports = {
    doPartida,
    doFase,
    doCopa,
    doOrganizarCopa,
}