// this loads esm modules and make possible use the import syntax without need compilation
// faster feedbacks, without dropping performance

require = require('esm')(module)

require('./api').default()