const path = require('path');
const { GenericContainer, Wait } = require("testcontainers");
const supertest = require('supertest')

describe('Container testing', () => {
    it('Should build and receive requests at /healthcheck', async () => {
        jest.setTimeout(300000);
        const buildContext = path.resolve(__dirname, "../");
        
        const container = await GenericContainer.fromDockerfile(buildContext);
        
        const startedContainer = await container
            .withEnv("PORT", "9000")
            .withExposedPorts(9000)
            .withWaitStrategy(Wait.forLogMessage("listening on 9000"))
            .start();

        const CONTAINER_IP = startedContainer.getContainerIpAddress()
        const CONTAINER_PORT = startedContainer.getMappedPort(9000)

        const url = `http://${CONTAINER_IP}:${CONTAINER_PORT}`
        
        const request = supertest(url)
        const { status } = await request.get('/healthcheck')
        
        expect(status).toBe(200)
        
        await startedContainer.stop();
    })
})
