const GUERRA_INFINITA = {
    id: 'tt4154756',
    titulo: 'Vingadores: Guerra Infinita',
    ano: 2018,
    nota: 8.8,
}

const OS_INCRIVEIS_2 = {
    id: 'tt3606756',
    titulo: 'Os Incríveis 2',
    ano: 2018,
    nota: 8.5,
}

const THOR_RAGNAROK = {
    id: 'tt3501632',
    titulo: 'Thor: Ragnarok',
    ano: '2017',
    nota: '7.9',
}

const JURASSIC_WORLD_REINO_AMEAÇADO = {
    id: 'tt4881806',
    titulo: 'Jurassic World: Reino Ameaçado',
    ano: 2018,
    nota: 6.7,
}

const DEADPOOL_2 = {
    id: 'tt5463162',
    titulo: 'Deadpool 2',
    ano: 2018,
    nota: 8.1,
}

const HAN_SOLO = {
    id: 'tt3778644',
    titulo: 'Han Solo: Uma História Star Wars',
    ano: 2018,
    nota: 7.2,
}

const HEREDITÁRIO = {
    id: 'tt7784604',
    titulo: 'Hereditário',
    ano: 2018,
    nota: 7.8,
}

const OITO_MULHERES_E_UM_SEGREDO = {
    id: 'tt5164214',
    titulo: 'Oito Mulheres e um Segredo',
    ano: 2018,
    nota: 6.3,
}

const TIMES = [ OS_INCRIVEIS_2,  JURASSIC_WORLD_REINO_AMEAÇADO, OITO_MULHERES_E_UM_SEGREDO, HEREDITÁRIO, GUERRA_INFINITA, DEADPOOL_2, HAN_SOLO, THOR_RAGNAROK]


module.exports = {
    GUERRA_INFINITA,
    OS_INCRIVEIS_2,
    THOR_RAGNAROK,
    JURASSIC_WORLD_REINO_AMEAÇADO,
    DEADPOOL_2,
    HAN_SOLO,
    HEREDITÁRIO,
    OITO_MULHERES_E_UM_SEGREDO,
    TIMES,
}