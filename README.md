# Backend NodeJS

A versão em NodeJS do backend do Copa Filmes.
Atualmente rodando em: https://api.copa-filmes.victorperin.ninja

## Endpoints

### GET /videos
Criado como um um wrapper para a [api original](https://copadosfilmes.azurewebsites.net/api/filmes), pois não não estava configurada com CORS.

Esse endpoint não recebe nenhum parametro nem autenticação.

### POST /copa
Processa a copa, recebendo no corpo da requisição, um array com 8 filmes e respondendo com 2.

Cada video precisa dos seguintes parametros:
```json
{
    "id": "string",
    "titulo": "string",
    "ano": number,
    "nota": number,
}
```

## Como rodar
Para rodar o projeto é super simples:
```shell
$ npm install
$ npm start
```

Você pode também especificar a variável de ambiente `$PORT`, que é referente à porta em que estiver rodando. Por padrão roda na porta 3000.

Você também pode rodar o projeto em uma imagem docker:
```shell
$ docker build -t copa-filmes-backend .
$ docker run -e PORT=80 -p 80:80 copa-filmes-backend
```

## Rodandos testes
O projeto tem 2 tipos de testes:
    - unitarios/integração
    - integração docker

### unitários/integração
Testes mais simples que rodam quase instantaneamente. São ideais para rodar enquanto estiver desenvolvendo.

Para roda-los, execute:
```shell
npm test
```

Ou se preferir, pode rodar no modo desenvolvimento, que executam conforme você edita os arquivos:
```shell
npm run dev:tests
```

### testes de integração no docker
Esses testes demoram mais para rodar, mas conseguem testar todo o ambiente, para ter certeza que tudo está funcionando dentro do container docker.

Para roda-los, execute:
```shell
npm run integration-tests
```

## Estrutura dos arquivos
```bash
.
├── config # toda configuração relacionada ao projeto e ambiente.
│   ├── custom-environment-variables.json # variaveis de ambiente
│   └── default.json # valores padrões
│     # Você pode adionar arquivos como production.json ou staging.json
│     # se quiser configurar especificamente para o ambiente
├── Dockerfile # arquivo usado para gerar a imagem do docker
├── gcloud # arquivos relacionados aos ambientes de staging e produção,
│   │      # hospedados no gcloud app engine
│   ├── app-staging.yaml
│   ├── app.yaml
│   └── dispatch.yaml
├── package.json # arquivo base para projeto NodeJS contendo pacotes,
│                # scripts e outras configurações relacionadas ao
│                # ambiente NodeJS
├── README.md # arquivo dessa codumentação
├── src # arquivos de source do projeto, tudo o que é executado em
│       # produção está contido aqui.
│   ├── api # arquivos base para construir api (sem regras de negocio)
│   │   ├── api.js # base da api, responsavel
│   │   ├── api.test.js # testes basicos de integração e checagem de rota.
│   │   ├── index.js
│   │   └── midlewares # middlewares compartilhados pro toda a api
│   │       ├── index.js
│   │       ├── koa-respond.js # status codes(pacote koa-respond)
│   │       └── routeAggregator.js # agregador de rotas, tudo o que está
│   │                              # no domain, é compartilhado aqui.
│   ├── domain # dominios da api
│   │   ├── copa
│   │   │   ├── handlers.js # funções de recebimento e resposta para a api
│   │   │   ├── routes.js # organização de rotas desse dominio
│   │   │   ├── services.js # regra de negócio do dominio
│   │   │   ├── services.test.js # testes de regra de negocio
│   │   │   └── validators.js # validadores de entrada/saida e mapeamento
│   │   └── filmes
│   │       ├── handlers.js
│   │       ├── routes.js
│   │       └── validators.js
│   └── index.js # arquivo inicial, tudo começa aqui
└── tests # pasta contendo testes avançados e arquivos para testes
    ├── docker.integration-test.js
    └── support # funções e dados padrões, para simplificar testes
        └── defaults.js # dados padrões
```

## Todo list
 - melhorar estrutura de pastas. (está bom, mas tem muito o que melhorar)
 - melhorar organização dos testes.
 - testes de carga.
 - adicionar [swagger](https://github.com/chuyik/koa-joi-router-docs) para documentação automatica da api.
 - fazer deploy usando o dockerfile e migrar do app engine (kubernetes e rancher)
 - otimizar gitlab-ci para rodar mais rápido.
 - mais testes negativos
 - git hooks
 - linter
 - mover para typescript

## Continous Integration
Estamos usando o GitLab CI para construir a aplicação. Nele, temos 3 passos:
  - build
  - test
  - deploy

### Build
O processo de build atualmente não faz muito, mas instala as dependencias e salva em cache para as proximas etapas (e para builds futuros)

### Test
Aqui temos 3 passos:
- unit_tests-node-latest
- unit_tests-node-10
- container_testing

Os 2 primeiors são responsáveis pelos testes unitários (em versões latest e LTS do node). Já o ultimo é referente a container testing, uma tática de rodar um ambiente completo dentro do docker e fazer requests fora do container.

Esses passos servem para certificar de que não tenhamos nenhum problema na hora do deploy, caso qualquer um deles falhe o ultimo passo não é executado.

### Deploy
Temos 2 possiveis casos:
- deploy_production: faz deploy para o google cloud engine de produção
- deploy_staging: faz o deploy para o google cloud engine de staging