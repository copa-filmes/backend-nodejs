FROM node:alpine

WORKDIR /srv/app
COPY package.json .
RUN npm install

COPY config/ ./config
COPY src/ ./src

CMD [ "node", "src/index.js" ]
